# gitlab_proof

[Verifying my OpenPGP key: openpgp4fpr:5993621FD9D214E212E421C3AC3A6CB9E464DBDA]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
